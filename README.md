# chat-ex
A simple chat application made with React and Socket.io

#Installation:
Clone the repo:

```
$ git clone https://github.com/kamiloc/chat-ex
```

Install npm dependencies:
```
$ npm install
```
And the dev dependencies:
```
$ npm install -dev
```

Run the development server with:
```
$ npm test
```

Now you can check the application on:
[Home](http://localhost:3000/)

